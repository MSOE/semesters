# Overview

This project is designed to provide information needed to specify quarter
to semester transition plans.

# Institution Level Files

The following files are maintained at the institution level:

* `programs.txt` &mdash; List of programs available
  Each line of the file specifies the folder and file name structure for each program.
  Note: program specializations are listed separately.
  Example:
  ```
  ae/AEe  Architectural Engineering (Electrical)
  ae/AEm  Architectural Engineering (Mechanical)
  ae/AEs  Architectural Engineering (Structural)
  as/AS   Actuarial Science
  ```
  Each program may have multiple tracks. For example, AS has:
  - `ASt2.txt` &mdash; Actuarial Science Transition in Year 2
  - `ASt3.txt` &mdash; Actuarial Science Transition in Year 2
  - `ASt4.txt` &mdash; Actuarial Science Transition in Year 2
  - `AS7.0.txt` &mdash; Actuarial Science V7.0 (full semester curriculum)
* `courseInfo.txt` &mdash; Exhaustive list of quarter-based and semester-based courses.
  This is the source of "ground-truth" for all courses.
  Each line has the following tab-separated fields:
  - Prefix
  - Code
  - Name
  - lecture hours
  - lab hours
  - credits
  - Prerequisite
  - Corequisite
  Requisite courses are specified in parentheses with `&` and `|` used as logical **and** and
  **or** specifiers, respectively.
* `allowMultiple.txt` &mdash; List of course codes for courses where credit may be received
  for multiple instances as long as the course titles differ.
* `sufficient.txt` &mdash; This provides a list of semester courses followed by one or more
  quarter courses that are a sufficient alternative to the semester course.
  Example:
  ```
  COM-1001,GS-1001
  MTH-1110,MA-137 | MA-137A | MA-2980
  ```
  The first line specifies that if a student has taken GS-1001, they do not need to take COM-1001.
  The second line specifies that if a student has taken MA-137 or MA-137H or MA-2980, they do not
  need to take MTH-1110. Note that even though MTH-1110 includes content from both MA-136 and MA-137,
  we only list MA-137 because, if a student has completed MA-137, they must have completed MA-136.
  Also, MA-2980 is the transition course for students who have completed MA-136 but not MA-137.
  Students who complete MA-2980 will have the content from MTH-1110 and be ready to take MTH-1120.
  Therefore, if a student has completed MA-137 or MA-2980, that is sufficient to receive credit
  for MTH-1110. Finally, consider:
  ```
  SWE-2710,SE-2800 & SE-2030
  ```
  In this case, a student may only receive credit for SWE-2710 if they have completed both
  SE-2800 and SE-2030 since SWE-2710 is a combination of the two courses.

# Program Level Files
The following files are maintained at the program level:
* Track files &mdash; These files have names like `ASt2.txt`, `AEt3e.txt`, and `NU8.0.txt` and
  specify model curriculum tracks that specify which courses should be taken each term. The
  transition tracks have names containing:
  - `t2` &mdash; 1 year of quarters and 3 years of semesters
  - `t3` &mdash; 2 years of quarters and 2 years of semesters
  - `t4` &mdash; 3 years of quarters and 1 year of semesters
  Each line has the following comma-separated fields:
  - Curriculum
  - Year
  - Term
  - Prefix
  - Code
  - MustHave &mdash; **Y** if this specific course or an alternative is required to graduate
  - Alternative &mdash; For `MustHave` courses, this lists an alternative course
  Example:  
  
  | Curriculum                            | Year | Term | Prefix | Code | MustHave | Alternative |
  | ---                                   | ---  | ---  | ---    | ---  | ---      | ---         |
  | Computer Science Transition in Year 2 | 1    |  1   |  EL    | CHBI |  N       |             |
  | Computer Science Transition in Year 2 | 1    |  1   |  GS    | 1001 |  Y       | COM-1001    |

  The first line indicates that a chemistry/biology elective is scheduled for the first quarter
  of the first year. Since `MustHave` is **N**, if this course is missed, no replacement course is
  required; however, the student will still need to earn the minimum number of credits in the
  the Math/Science area (since CS requires a minimum of 29 Math/Science credits). See below for
  details on miminum credit counts in specific areas.  
  The second line indicates that if GS-1001 is not completed, then COM-1001 must be completed
  instead.
  Here are some other examples:

  | Curriculum                            | Year | Term | Prefix | Code | MustHave | Alternative |
  | ---                                   | ---  | ---  | ---    | ---  | ---      | ---         |
  | Computer Science Transition in Year 2 | 2    |  1   |  CSC   | 2611 |  Y       | CS-2300     |

  This may seem counter-intutive since this is a semster-based course. The alternative entry
  is included to account for the situation where the student may have already completed CS-2300
  under quarters. In this case, the student should not be required to take CSC-2611.

  | Curriculum                                | Year | Term | Prefix | Code | MustHave | Alternative       |
  | ---                                       | ---  | ---  | ---    | ---  | ---      | ---               |
  | Software Engineering Transition in Year 2 | 2    |  2   |  SWE   | 2710 |  Y       | SE-2800 & SE-2030 |

  This indicates that in order to receive credit for SWE-2710, the student must complete both
  SE-2800 and SE-2030 under quarters. If the student has completed only one of these two courses,
  s/he will still be required to take SWE-2710.

  | Curriculum                             | Year | Term | Prefix | Code | MustHave | Alternative |
  | ---                                       | ---  | ---  | ---    | ---  | ---      | ---      |
  | Software Engineering Transition in Year 2 | 3    |  2   |  SWE   | 4511 |  Y       |          |

  This indicates that SWE-4511 is required to graduate but that there is no comparable quarter-based
  course.
* `sufficient.txt` &mdash; This file is optional and is only required if the program wishes to
  override any of the sufficency mappings specified at the institutional level.
* `Credits-Total.txt` and other `Credits-` files &mdash; Each program must specify the total
  number of semester-based credits required for graduation. The `Credits-Total.txt` file contains
  just this number. Additional `Credits-` files may be specified to indicate other credit minimums
  that students must achieve in specific content areas. For example, for ABET accreditation,
  engineering programs require a minimum of 30 math/science credits and 45 engineering credits.
  Programs specify these with additional files. For example, software engineering specifies
  two content areas with `Credits-MathScience.txt` and `Credits-Program.txt`. The first line
  of the file specifies the minimum number of semester-based credits required and the name of
  the content area. The remaining lines specify what courses may be used to meet the content
  area requirements. These courses may be specified with wildcards and exclusion lists.
  Example (`Credits-MathScience.txt`):
  ```
  31 Math/Science
  MA-**
  -MA-120,Precalculus Mathematics
  -MA-1204,Quantitative Reasoning for Health Care Professionals
  -MA-125,College Algebra I
  ```
  The second line indicates that all courses with a `MA` prefix can be used to meet this
  requirement. The remaining three lines specify that MA-120, MA-1204, and MA-125 should be
  explicitly excluded from the list (because of the minus sign at the beginning of the lines).  
  The course titles are ignored by the software and not required. Programs can create as many
  content areas with credit minimums as they would like; however, the content areas need to be
  mutually exclusive.
* Elective lists &mdash; The curriculum tracks specify electives. For each elective type, the
  program must provide a list of courses that fulfill the elective requirement. These files
  are named based on the elective specification. For example, if a curriculum track has `EL-HU`
  as an elective, a file named `EL-HU.txt` contains a list of all the courses that qualify.
  Example (`ELC-Ethi.txt`):
  ```
  # ELC-Ethi
  PHL-3101,Ethics for Professional Managers and Engineers
  PHL-3102,Bioethics
  PHL-3103,Ethics of Digital Technologies and Artificial Intelligence
  ```
  Again, course titles are ignored by the software and are not required.

# Program File Status

## Mostly complete
These should be in good enough shape to use.
* AE - MustHaves and Elective lists need to be vetted
* AS - MustHaves and Elective lists need to be vetted
* CE - MustHaves and Elective lists need to be vetted
* CM - MustHaves and Elective lists need to be vetted
* CS
* CV - MustHaves and Elective lists need to be vetted
* SE

## Todo
The following programs still need significantly more data collecting work.
* BA
* BE
* BIOE
* EE
* IE
* NU
* UX
